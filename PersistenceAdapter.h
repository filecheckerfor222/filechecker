//
//  PersistenceAdapter.h
//  filechecker
//
//  Created by Jason Zhou on 14-4-30.
//  Copyright (c) 2014年 Jiasheng Zhou. All rights reserved.
//

#ifndef __filechecker__PersistenceAdapter__
#define __filechecker__PersistenceAdapter__
#include <mysql_connection.h>
#include <mysql_driver.h>
#include <cppconn/exception.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/driver.h>
#include <mysql.h>
#include "Record.h"
#include <string>
using namespace std;
class PersistenceAdapter
{
private:
    static sql::Connection *ipConn;
    static sql::Driver *driver;
    void makeConnection();
    
public:
    PersistenceAdapter();
    DirFileRecord* loadFromDB(const char* dirname, map<String,DirFileRecord*>&index);
    void saveToDB(DirFileRecord *);
    void deleteAnyExistingData(const char* dirname);
    
    void addServer(string name,string ipaddress);
    void removeSever(string name);
    string getServerIP(string name);
    
    void loadDirectoryList(List<string> &alist);
    void loadServerList(map<string,string> &servers);
};
￼

#endif /* defined(__filechecker__PersistenceAdapter__) */
