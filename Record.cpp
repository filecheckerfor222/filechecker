//
//  Record.cpp
//  222A1
//
//  Created by Rebecca on 14-4-30.
//  Copyright (c) 2014年 Rebecca. All rights reserved.
//

#include "Record.h"


Record::Record(char * name, Record * parent)
{
    int max=parent->getID();
    this->name=name;
    this->parent=parent;
    children = new vector<RecordPtr>;
    vector<RecordPtr> *c=parent->getChildren();
    if (c->size()!=0) {
        vector<RecordPtr>::iterator it;
        for (it=c->begin(); it!=c->end(); it++)
        {
            if ((*it)->getID()>max)
            {
                max=(*it)->getID();
            }
        }
        
    }
    max++;
    this->id=max;
    computeHashKey();
    
}
void Record::addChild(Record *achild)
{
    children->push_back(achild);
}
//override two functions

void DirFileRecord::computeHashKey()
{
    
    rtype = DirFile;
    
}

DirFileRecord::~DirFileRecord(){
    
}
void RegFileRecord::computeHashKey(){
    rtype = RegFile;
}
RegFileRecord::~RegFileRecord(){
    
}
void SymLinkRecord::computeHashKey(){
    struct stat info;
    lstat(name, &info);
    
    rtype=SymLink;
}
SymLinkRecord::~SymLinkRecord(){
    
}
void OtherFileRecord::computeHashKey(){
    rtype=OtherFile;
}
OtherFileRecord::~OtherFileRecord(){
    
}

