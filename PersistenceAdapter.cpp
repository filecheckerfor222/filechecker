//
//  PersistenceAdapter.cpp
//  filechecker
//
//  Created by Jason Zhou on 14-4-30.
//  Copyright (c) 2014年 Jiasheng Zhou. All rights reserved.
//

#include <iostream>
#include "PersistenceAdapter.h"

using namespace std;

PersistenceAdapter::PersistenceAdapter()
{
    driver = get_driver_instance();
    try {
        ipConn = driver->connect("tcp://127.0.0.1::3306", "admin", "password");
    } catch (sql::SQLException &e) {
        return;
    }
}


void PersistenceAdapter::addServer(string name, string ipaddress)
{
    
  
    ipConn->setSchema("servers");
    string insertServer = "insert into servers values (?,?)";
    
    sql::PreparedStatement *pstmt = NULL;
    
    pstmt = ipConn->prepareStatement(insertServer);
    
    pstmt->setString(1, name);
    pstmt->setString(2, ipaddress);
    pstmt->executeUpdate();
    delete pstmt;

    
}

void PersistenceAdapter::removeSever(string name)
{
    ipConn->setSchema("servers");
    
    string deleteServer = "delete from servers where name=?";
    sql::PreparedStatement *pstmt = NULL;
    pstmt = ipConn->prepareStatement(deleteServer);
    
    pstmt->setString(1, name);
    pstmt->executeUpdate();
    delete pstmt;

}

string PersistenceAdapter::getServerIP(string name)
{

    
    ipConn->setSchema("servers");
    
    string getServer = "select ipaddress from servers where name=?";
    sql::PreparedStatement *pstmt = NULL;
    sql::ResultSet *rs = NULL;
    pstmt = ipConn->prepareStatement(getServer);
    
    pstmt->setString(1, name);
    rs = pstmt->executeQuery();
    if (!rs->next()) {
        delete pstmt;
        delete rs;
        return NULL;
    }
    string ip = rs->getString(1);
    delete pstmt;
    
    delete rs;
    return ip;
    
    
}

void PersistenceAdapter::loadServerList(map<string, string> &servers)
{
    for (map<string, string>::iterator itr = servers.begin(); itr!=servers.end(); itr++) {
        addServer(itr->first, itr->second);
    }
    
    
}