//
//  Record.h
//  222A1
//
//  Created by Rebecca on 14-4-30.
//  Copyright (c) 2014年 Rebecca. All rights reserved.
//

#ifndef ___22A1__Record__
#define ___22A1__Record__

#include <iostream>
#include <vector>
#include <functional>
#include <sys/stat.h>
using namespace std;

class Record;
typedef Record * RecordPtr;
enum Rectype { RegFile, SymLink,OtherFile,DirFile};
class Record
{
protected:
    char * name;
    int id;
    Record * parent;
    int hash;
    vector <Record*> * children;
    enum Rectype rtype;
    
public:
    Record(char * name, Record * parent);
    void addChild(Record * achild);
    virtual ~Record();
    virtual void computeHashKey();
    
    //get and sets
    
    char* getName(){return this->name;};
    int getID(){return this->id;};
    RecordPtr getParent(){return this->parent;};
    int getHash(){return this->hash;};
    vector <Record*> * getChildren(){return this->children;};
    enum Rectype getType(){return this->rtype;};
    
    void setName(char * name){this->name=name;};
    void setID(int id){this->id=id;};
    void setParent(RecordPtr parent){this->parent=parent;};
    void setHash(int hash){this->hash=hash;};
    void setType(enum Rectype rtype){this->rtype=rtype;};
    
};

class DirFileRecord : Record
{
public:
    void computeHashKey();
    ~DirFileRecord();
};

class RegFileRecord : Record
{
public:
    void computeHashKey();
    ~RegFileRecord();
};
class SymLinkRecord : Record
{
public:
    void computeHashKey();
    ~SymLinkRecord();
    
};
class OtherFileRecord : Record
{
public:
    void computeHashKey();
    ~OtherFileRecord();
};








#endif /* defined(___22A1__Record__) */
